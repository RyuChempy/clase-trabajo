﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Security.Cryptography;
using UnityEngine;

public class CapsuleController: MonoBehaviour
{
    [SerializeField]
    private Vector3 direction;
    public float speed;
    // Update is called once per frame
    void Update()
    {
        //Acotacion de los vectores de direccion al valor unitario [-1,1]
        direction = ClampVector3(direction);
        // Desplazamiento del componente transform en base al tiempo
        transform.Translate(direction * (speed * Time.deltaTime));
    }
    public static Vector3 ClampVector3(Vector3 target)
    {
        float clampedX = Mathf.Clamp(target.x, -1f, 1f);
        float clampedY = Mathf.Clamp(target.x, -1f, 1f);
        float clampedZ = Mathf.Clamp(target.x, -1f, 1f);

        Vector3 result = new Vector3(clampedX, clampedY, clampedZ);
        return result;
    }
}
