﻿using System.Threading;

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CapsuleScaling : MonoBehaviour
{
    [SerializeField]
    private Vector3 axes;    //Posibles ejes de escalado
    public float scaleUnit;  //vatiedad de escalado

    // Update is called once per frame
    void Update()
    {
        //Acotacion del escalado al valor unitario (1,1)
        axes = CapsuleMovement.ClampVector3(axes);

        //La escala , al contrario de la rotacion y el movimiento, es acumulativa 
        //Lo que quiere decir que debemos aadir un nuevo valor de la escala , de la anterior
        transform.localScale += axes * (scaleUnit * Time.deltaTime);
        
    }
}
