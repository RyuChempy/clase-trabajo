﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CapsuleRotation : MonoBehaviour
{
    [SerializeField]
    private Vector3 direction;
    public float speed;
    // Update is called once per frame
    void Update()
    {
        direction = CapsuleMovement.ClampVector3(direction);

        transform.Translate(direction * (speed * Time.deltaTime));

        
    }
}
